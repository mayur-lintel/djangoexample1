from __future__ import unicode_literals

from django.conf import settings
from django.db import models

# Create your models here.


class User(models.Model):
    name = models.CharField(max_length=200)
    gender_list = (('M', 'Male'), ('F', 'Female'))
    gender = models.CharField(max_length=1, choices=gender_list)
    mobile_no = models.CharField(max_length=10)
    email_id = models.CharField(max_length=300)
    password = models.CharField(max_length=25, default="")


class Contact_List(models.Model):
    contact_from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='contact_from_user_id', null=True)
    contact_to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='contact_to_user_id', null=True)
    status_list = (('r', 'request'), ('f', 'friend'))
    status = models.CharField(max_length=1, choices=status_list)
    message = models.TextField()
    created_date = models.DateTimeField(auto_now=True)
    updated_date = models.DateTimeField(auto_now=True)


class Chat(models.Model):
    chat_from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='chat_from_user_id', null=True)
    chat_to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='chat_to_user_id', null=True)
    message = models.TextField()
    created_date = models.DateTimeField(auto_now=True)
    updated_date = models.DateTimeField(auto_now=True)


class Share(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_id')
    like = models.ManyToManyField(User, related_name='like', through='Share_Like')
    comment = models.ManyToManyField(User, related_name='comment', through='Share_Comment')
    message = models.TextField()
    image = models.FileField(upload_to=settings.STATIC_URL+'uploads/')
    created_date = models.DateTimeField(auto_now=True)
    updated_date = models.DateTimeField(auto_now=True)


class Share_Like(models.Model):
    share = models.ForeignKey(Share, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)


class Share_Comment(models.Model):
    share = models.ForeignKey(Share, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.TextField()
    created_date = models.DateTimeField(auto_now=True)
    updated_date = models.DateTimeField(auto_now=True)

