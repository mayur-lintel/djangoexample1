import os
import re

from django.core.urlresolvers import reverse

from django.http import HttpResponseRedirect
from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
import urls

from .models import *


@csrf_exempt
def login(request):
    if request.session.get('uid'):
        return HttpResponseRedirect(reverse('signupApp:home'))
    elif request.POST:
        email = request.POST['txt_email']
        password = request.POST['txt_password']
        flag = True

        error_msg = []
        if email == "":
            error_msg.append("Email Address is required.")
            flag = False

        if password == "":
            error_msg.append("Password is required.")
            flag = False

        if flag:
            if User.objects.filter(email_id=email):
                u = User.objects.get(email_id=email)
                if u.password == password:
                    request.session['uid'] = u.id
                    print u.get_gender_display()
                    return HttpResponseRedirect(reverse('signupApp:home'))
                else:
                    return render(request, 'signupApp/login.html',
                                  {'error_message': "Email Address or Password is incorrect"})
            else:
                return render(request, 'signupApp/login.html', {'error_message':
                                                                "Email Address or Password is incorrect"})
        else:
                return render(request, 'signupApp/login.html', {'error_msg': error_msg})
    else:
        return render(request, "signupApp/login.html")


@csrf_exempt
def signup(request):
    if request.session.get('uid'):
        return HttpResponseRedirect(reverse('signupApp:home'))
    if request.POST:
        flag = True
        name = request.POST['txt_name'].lstrip()
        mobileno = request.POST['txt_mobileno'].lstrip()
        email = request.POST['txt_email'].lstrip()
        password = request.POST['txt_password'].lstrip()
        confirm_password = request.POST['txt_confirm_password'].lstrip()
        error_message = []
        # print(request.POST)

        # print User.objects.get(email_id="mayur@lintelndia.com")

        if name == "":
            error_message.append("Name is required.")
            flag = False
        elif not re.match(r'^[a-zA-Z ]+$', name):
            error_message.append("Name contains only alphabetic character.")
            flag = False

        if 'rbn_gender' not in request.POST:
            error_message.append("Gender is required.")
            flag = False
        else:
            gender = request.POST['rbn_gender']

        if mobileno == "":
            error_message.append("Mobile No is required.")
            flag = False
        elif not re.match(r'^[0-9]{10}$', mobileno):
            error_message.append("Invalid Mobile No.")
            flag = False
        elif User.objects.filter(mobile_no=mobileno):
            error_message.append("Mobile No is already registered.")
            flag = False

        if email == "":
            error_message.append("Email is required.")
            flag = False
        elif not re.match(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', email):
            error_message.append("Invalid Email Address.")
            flag = False
        elif User.objects.filter(email_id=email):
            error_message.append("Email Id is already registered.")
            flag = False

        if password == "":
            error_message.append("Password is required.")
            flag = False
        elif not re.match(r'^.{8,15}$', password):
            error_message.append("Password length must be between 8 to 15 characters.")
            flag = False
        elif password != confirm_password:
            error_message.append("Password & Confirm Password must be same.")
            flag = False

        if flag:
            u = User(name=name, gender=gender, mobile_no=mobileno, email_id=email, password=password)
            u.save()
            path = settings.BASE_DIR + '/signupApp' + settings.STATIC_URL + 'signupApp/UserData/' + '%d.txt' % u.id
            f = open(path, 'w')
            f.write("Id:- %d\nName:- %s\nGender:- %s\nMobile No:- %s\nEmail Id:-%s\nPassword:- ******"
                    % (u.id, name, dict(u.gender_list)[gender], mobileno, email))
            f.close()
            return render(request, "signupApp/signup.html", {'success_message': "User Register Successfully."})
            # return HttpResponseRedirect(reverse('signupApp:signup', args=("User Register Successfully.",)))
        else:
            return render(request, "signupApp/signup.html", {'error_message': error_message,
                                                             'view_state': dict(request.POST)})
    else:
        return render(request, "signupApp/signup.html")


def home(request):
    if not request.session.get('uid'):
        return HttpResponseRedirect(reverse('signupApp:login'))
    else:
        uid = request.session['uid']
        u = User.objects.get(pk=uid)
        path = settings.BASE_DIR + '/signupApp' + settings.STATIC_URL + 'signupApp/UserData/' + '%d.txt' % u.id
        if os.path.isfile(path):
            f = open(path, 'r')
            content = f.read()
        else:
            content = "Sorry, currently no data available.."
        return render(request, "signupApp/home.html", {"content": content})


def contacts(request):
    from django.core.urlresolvers import RegexURLResolver, RegexURLPattern

    view_list = []

    def get_all_view_names(urlpatterns):
        for pattern in urlpatterns:
            if isinstance(pattern, RegexURLResolver):
                get_all_view_names(pattern.url_patterns)
            elif isinstance(pattern, RegexURLPattern):
                print pattern.callback.func_name
                view_name = pattern.callback.func_name
                view_list.append(view_name)

    # get_all_view_names(urls.urlpatterns)

    def show_urls(urllist):
        for entry in urllist:
            view_list.append(entry.callback.func_name)
            if hasattr(entry, 'url_patterns'):
                show_urls(entry.url_patterns)
        return view_list

    show_urls(urls.urlpatterns)

    print view_list

    if not request.session.get('uid'):
        return HttpResponseRedirect(reverse('signupApp:contacts'))
    else:
        uid = request.session['uid']
        u = User.objects.get(pk=uid)
        contact_lst = Contact_List.objects.raw('select * from signupApp_contact_list WHERE (contact_from_user_id = %s or contact_to_user_id = %s)', [u.id, u.id])
        for c in contact_lst:
            print c.message
        return render(request, "signupApp/contacts.html")


def logout(request):
    if not request.session.get('uid'):
        return HttpResponseRedirect(reverse('signupApp:login'))
    else:
        del request.session['uid']
        return HttpResponseRedirect(reverse('signupApp:login'))



