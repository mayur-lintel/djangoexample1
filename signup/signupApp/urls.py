from django.conf.urls import url
from . import views

app_name = "signupApp"
urlpatterns = [
    url(r'^login/$', views.login, name="login"),
    url(r'^signup/$', views.signup, name="signup"),
    url(r'^home/$', views.contacts, name="home"),
    url(r'^contacts/$', views.contacts, name="contacts"),
    url(r'^logout/$', views.logout, name="logout"),
]