from .models import User


def base(request):
    if not request.session.get('uid'):
        return {"name": ''}
    else:
        uid = request.session['uid']
        u = User.objects.get(pk=uid)
        return {"name": u.name}
