user


contact_list
	id
	from_uid
	to_uid
	status
	message
	created_date
	updated_date
	
chat
	id
	from_uid
	to_uid
	message
	created_date
	updated_date

share
	id
	uid
	message
	image
	created_date
	updated_date
	total_view
	
share_like
	share_id
	uid
	created_date

share_comment
	id
	share_id
	uid
	comment
	created_date
	updated_date
	
notification
	id
	uid
	title
	description
	created_date

notification_detail
	id
	to_uid
	read_status
	created_date
	updated_date
	

